# RDFAnonymization

## Content

- TDBDataset.jar
- FusekiServerManager.jar

## User manual

### TDBDataset: Use to split data into two dataset (public and private).
- Usage: java -jar TDBDataset.jar <query_file_path> <--true/false>
	+ query_file_path: file contain select query (select the attributes to put into private dataset)
	+ --true/false: true for anonymized file and false for non-anonymized file

### FusekiServerManager: Use to illustrate dataset, manage data on server.
- Usage: java -jar FusekiManagerServer.jar <role> <access_dataset> <query_file_path> [dataset_type] [non_anonymized_file_path]
	+ role: user role to access server (-dba or -dev/--developer)
	+ access_dataset: choose dataset to access (--public or --private or --all)
	+ query_file_path: queries to execute
	+ dataset_type: type of dataset non-anonymized (-na or -NA) or anonymized (-a or -A)
	+ non_anonymized_file_path: non anonymized file path. (this can not be empty if dataset_type is -na or -NA)
