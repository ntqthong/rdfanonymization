import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.sparql.core.TriplePath;
import org.apache.jena.sparql.syntax.*;
import org.apache.jena.tdb.TDBFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class TDBDataset {
    private static final String DIRECTORY = "./tdb/ds1";
    private static final String TMP_DIRECTORY = "./tdb/tmp";
    private static final String SOURCE = "./dataset/anonymized_religions_qids/k_3/lubm1.ttl";
    private static final String PUBLIC_DATASET = "./dataset/public_dataset.ttl";
    private static final String PRIVATE_DATASET = "./dataset/private_dataset.ttl";
    private static final String ID_FILE = "./dataset/id.rq";
    private static final String SA_FILE = "./dataset/sa.rq";

    private static final String PREFIX = "prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                                        "prefix owl:   <http://www.w3.org/2002/07/owl#>\n" +
                                        "prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\n" +
                                        "prefix ub:    <http://swat.cse.lehigh.edu/onto/univ-bench.owl#>\n";

    private static final ArrayList<String> subjectTypes = new ArrayList<>();
    private static final Dataset dataset = createTDBDataset(SOURCE, DIRECTORY);
    private static boolean alreadyAnonymized = false;

    public static void main(String[] args) throws IOException {

        long startTime, stopTime;
        var evaluation = new EvaluationFuseki();

        Map<String, ArrayList<String>> results = new HashMap<>();
        dataset.begin(ReadWrite.READ);

        if (args.length != 0) {
            switch(args[1]) {
                case "--true":
                    alreadyAnonymized = true;
                    results = fileReader(args[0], results);
                    break;
                case "--false":
                    results = fileReader(args[0], results);
                    break;
            }
        } else {
            System.out.println("[file path with queries] [--true/--false if the dataset is already anonymized]");
            return;
        }
        Model model = ModelFactory.createDefaultModel();
        startTime = System.currentTimeMillis();
        model.read(SOURCE);
        stopTime = System.currentTimeMillis();
        evaluation.setExecuteTimeOrigin(stopTime - startTime);

        startTime = System.currentTimeMillis();
        Model publicModel = createPublicModel(results, model);
        stopTime = System.currentTimeMillis();
        evaluation.setExecuteTimePublic(stopTime - startTime);

        startTime = System.currentTimeMillis();
        Model privateModel = createPrivateModel(results);
        stopTime = System.currentTimeMillis();
        evaluation.setExecuteTimePrivate(stopTime - startTime);

        evaluation.setOriginTriples(model.size());
        evaluation.setPublicTriples(publicModel.size());
        evaluation.setPrivateTriples(privateModel.size());

        OutputStream outPublic = new FileOutputStream(PUBLIC_DATASET);
        OutputStream outPrivate = new FileOutputStream(PRIVATE_DATASET);
        RDFDataMgr.write(outPublic, publicModel, Lang.TURTLE);
        RDFDataMgr.write(outPrivate, privateModel, Lang.TURTLE);
        //addQuery(ID_FILE, "SELECT ?s WHERE {?s rdf:type ub:FullProfessor.}");
        //addQuery(ID_FILE, "SELECT ?s WHERE {?s rdf:type ub:AssistantProfessor.}");
        //addQuery(SA_FILE, "SELECT ?s ?r WHERE {?s rdf:type ub:FullProfessor. ?s ub:hasReligion ?r.}");
        //addQuery(SA_FILE, "SELECT ?x ?y WHERE {?x rdf:type ub:AssistantProfessor. ?x ub:hasReligion ?y.}");

        evaluation.setOriginSize(Files.size(Path.of(SOURCE)));
        evaluation.setPublicSize(Files.size(Path.of(PUBLIC_DATASET)));
        evaluation.setPrivateSize(Files.size(Path.of(PRIVATE_DATASET)));

        System.out.println(evaluation.toString());
    }

    public static Map<String, ArrayList<String>> fileReader(String path, Map<String, ArrayList<String>> results) throws FileNotFoundException {
        Scanner sc = new Scanner (new File(path));
        String prefix = "";
        while (sc.hasNext()) {
            String line = sc.nextLine();
            if (line.contains("prefix")) {
                prefix = prefix.concat(line + "\n");
            }
            if (line.contains("SELECT")) {
                if (results.isEmpty()) {
                    results = getResultFromQuery(dataset, prefix + line);
                } else {
                    Map<String, ArrayList<String>> tmp = getResultFromQuery(dataset, prefix + line);
                    for (var key : tmp.keySet()) {
                        if (!results.containsKey(key)) {
                            results.put(key, tmp.get(key));
                        } else {
                            for (var element : tmp.get(key)) {
                                results.get(key).add(element);
                            }
                        }
                    }
                }
            }
        }
        return results;
    }

    public static Dataset createTDBDataset(String source, String directory) {
        Dataset dataset = TDBFactory.createDataset(directory);
        Model model = ModelFactory.createDefaultModel();
        Model ressources = model.read(source);
        dataset = DatasetFactory.create(ressources);
        return dataset;
    }

    private static Map<String, ArrayList<String>> executeQuery(Dataset dataset, String query) {
        Query tmp = QueryFactory.create(query);
        ElementWalker.walk(tmp.getQueryPattern(),
                new ElementVisitorBase() {
                    public void visit(ElementPathBlock el) {
                        Iterator<TriplePath> triples = el.patternElts();
                        if (triples.hasNext()) {
                             subjectTypes.add(triples.next().getObject().toString());
                        }
                    }
                }
        );

        try (QueryExecution queryExec = QueryExecutionFactory.create(query, dataset)) {
            Map<String, ArrayList<String>> resultMap = new HashMap<>();
            ResultSetRewindable results = queryExec.execSelect().rewindable();
            ResultSetFormatter.out(results);
            results.reset();
            while (results.hasNext()) {
                QuerySolution binding = results.nextSolution();
                Iterator<String> varNames = binding.varNames();
                ArrayList<String> qid = new ArrayList<>();
                RDFNode id = binding.get(varNames.next());
                if (alreadyAnonymized) {
                    qid = findRealValues(query, id);
                } else {
                    while (varNames.hasNext()) {
                        RDFNode element = binding.get(varNames.next());
                        qid.add(rdfNodeDetails(element));
                    }
                }
                resultMap.put(id.toString(), qid);
            }
            return resultMap;
        }
    }

    private static ArrayList<String> findRealValues(String query, RDFNode id) {
        ArrayList<String> qid = new ArrayList<>();
        Dataset tmpDataset;
        if (query.contains("ub:age")) {
            tmpDataset = createTDBDataset("./dataset/quasi_identifier_attributes/age_file.ttl", TMP_DIRECTORY);
            qid.add(executeTmpQuery(id, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#age", tmpDataset));
        }
        if (query.contains("ub:hasReligion")) {
            tmpDataset = createTDBDataset("./dataset/sensitive_attributes/religions_1univ.ttl", TMP_DIRECTORY);
            qid.add(executeTmpQuery(id, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#hasReligion", tmpDataset));
        }
        if (query.contains("ub:zipcode")) {
            tmpDataset = createTDBDataset("./dataset/quasi_identifier_attributes/zipcodes.ttl", TMP_DIRECTORY);
            qid.add(executeTmpQuery(id, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#zipcode", tmpDataset));
        }
        if (query.contains("ub:politics")) {
            tmpDataset = createTDBDataset("./dataset/sensitive_attributes/politics_1univ.ttl", TMP_DIRECTORY);
            qid.add(executeTmpQuery(id, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#politics", tmpDataset));
        }
        if (query.contains("ub:sex")) {
            tmpDataset = createTDBDataset("./dataset/quasi_identifier_attributes/sex_file.ttl", TMP_DIRECTORY);
            qid.add(executeTmpQuery(id, "http://swat.cse.lehigh.edu/onto/univ-bench.owl#sex", tmpDataset));
        }
        return qid;
    }

    private static String executeTmpQuery(RDFNode id, String predicate, Dataset tmpDataset) {
        String tmpQuery = PREFIX + "SELECT ?x WHERE {<" + id.toString() + "> <"+ predicate  + "> ?x.}";
        String sol = "";
        try (QueryExecution queryExec = QueryExecutionFactory.create(tmpQuery, tmpDataset)) {
            ResultSetRewindable results = queryExec.execSelect().rewindable();
            if (results.hasNext()) {
                QuerySolution binding = results.nextSolution();
                RDFNode element = binding.get("?x");
                sol =  rdfNodeDetails(element, "=" + predicate);
            }
            return sol;
        }
    }

    private static String rdfNodeDetails (RDFNode node, String... predicate) {
        String type = "";
        if (node.isLiteral()) {
            type = "l:";
        }
        if (node.isResource()) {
            type =  "r:";
        }
        if (predicate.length > 0) {
            return type + node.toString() + predicate[0];
        }
        return type + node.toString();
    }

    private static int checkType(String node) {
        for (int i = 0; i < subjectTypes.size(); i++) {
            if (node.contains(subjectTypes.get(i).split("#")[1])) {
                return i;
            }
        }
        return -1;
    }

    private static Map<String, ArrayList<String>> anonymizedResult(Map<String, ArrayList<String>> resultMap) {
        char letter;
        String id;
        Random rand = new Random();
        Map<String, ArrayList<String>> anonymizedMap = new HashMap<>();
        for (var result : resultMap.keySet()) {
            if (checkType(result) == -1) {
                letter = 'z';
            }
            else {
                letter = (char) (97 + 25 - checkType(result));
            }
            id = result + "=http://" + letter + rand.nextInt(1000);
            anonymizedMap.put(id, resultMap.get(result));
        }
        return anonymizedMap;
    }

    public static Map<String, ArrayList<String>> getResultFromQuery(Dataset dataset, String query) {
        Map<String, ArrayList<String>> resultMap = executeQuery(dataset, query);
        return anonymizedResult(resultMap);
    }

    private static Model createPublicModel(Map<String, ArrayList<String>> resultMap, Model model) {
        Model newModel = ModelFactory.createDefaultModel();
        Iterator<Statement> iterator = model.listStatements();
        String uri, anonymizedUri;
        Resource resource;
        boolean qidIsPresent = false;
        boolean subjectIsAnonymized = false;
        newModel.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        newModel.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
        newModel.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        newModel.setNsPrefix("ub", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#");

        while(iterator.hasNext()) {
            var statement = iterator.next();
            var subject = statement.getSubject();
            var predicate = statement.getPredicate();
            var object = statement.getObject();
            for (var key : resultMap.keySet()) {
                uri = key.split("=", 2)[0];
                if (uri.equals(subject.toString())) {
                    anonymizedUri = key.split("=", 2)[1];
                    resource = newModel.createResource(anonymizedUri);
                    ArrayList<String> qid = resultMap.get(key);
                    if (!qid.isEmpty()) {
                        for (int i = 0; i < qid.size(); i++) {
                            if (alreadyAnonymized) {
                                if (qid.get(i).split("=", 2)[0].substring(2).equals(object.toString())) {
                                    qidIsPresent = true;
                                    break;
                                }
                            }
                            if (qid.get(i).substring(2).equals(object.toString())) {
                                qid.set(i, qid.get(i).concat("=" + predicate.toString()));
                                qidIsPresent = true;
                                break;
                            }
                        }
                    }
                    if (qidIsPresent) {
                        qidIsPresent = false;
                        subjectIsAnonymized = true;
                        break;
                    } else {
                        newModel.add(newModel.createStatement(resource, predicate, object));
                        subjectIsAnonymized = true;
                    }
                }
            }
            if (subjectIsAnonymized) {
                subjectIsAnonymized = false;
            } else {
                newModel.add(newModel.createStatement(subject, predicate, object));
            }
        }
        return newModel;
    }

    private static Model createPrivateModel(Map<String, ArrayList<String>> resultMap) {
        Model model = ModelFactory.createDefaultModel();
        String uri, anonymizedUri;
        model.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        model.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
        model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        model.setNsPrefix("ub", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#");

        for (var key : resultMap.keySet()) {
            uri = key.split("=", 2)[0];
            anonymizedUri = key.split("=", 2)[1];
            Resource uriResource = model.createResource(uri);
            Property sameAsProperty = model.createProperty("http://www.w3.org/2002/07/owl#sameAs");
            Resource uriAnonymizedResource = model.createResource(anonymizedUri);
            uriResource.addProperty(sameAsProperty, uriAnonymizedResource);
            ArrayList<String> qid = resultMap.get(key);
            if (!qid.isEmpty()) {
                for (var element : qid) {
                    System.out.println(element);
                    if (element.contains("=")) {
                        Property property = model.createProperty(element.split("=", 2)[1]);
                        char type = element.charAt(0);
                        String object = element.substring(2, element.length()-1).split("=", 2)[0];
                        if (type == 'l') {
                            Literal literal = model.createLiteral(object);
                            uriResource.addProperty(property, literal);
                        }
                        if (type == 'r') {
                            Resource resource = model.createProperty(object);
                            uriResource.addProperty(property, resource);
                        }
                    }
                }
            }
        }
        return model;
    }

    private static File createQueryFile(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(PREFIX);
            fileWriter.write("\n");
            fileWriter.close();
            System.out.println("File created: " + file.getName());
            return file;
        }
        return file;
    }

    private static void addQuery(String path, String query) throws IOException {
        FileWriter writer = new FileWriter(createQueryFile(path), true);
        writer.write(query);
        writer.write("\n");
        writer.close();
    }
}