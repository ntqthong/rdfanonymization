import org.apache.commons.io.FileUtils;
import org.apache.jena.fuseki.main.FusekiServer;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.rdfconnection.RDFConnectionRemote;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FusekiServerManager {

    private static final String PUBLIC_DIRECTORY = "./tdb/public";
    private static final String PRIVATE_DIRECTORY = "./tdb/private";
    private static final String PUBLIC_DATASET = "./dataset/public_dataset.ttl";
    private static final String PRIVATE_DATASET = "./dataset/private_dataset.ttl";
    private static final String PUBLIC_SERVER = "/public";
    private static final String PRIVATE_SERVER = "/private";
    private static final int PORT = 3330;

    public static void main(String[] args) throws FileNotFoundException {
        String fileType = "";
        String nonAnonymizedFile = "";
        if (args.length < 4) {
            fileType = "-a";
        }
        else {
            fileType = args[3];
            if (args[4].length() != 0) {
                nonAnonymizedFile = args[4];
            }
        }
        /*
        String q = "prefix ub:    <http://swat.cse.lehigh.edu/onto/univ-bench.owl#>\n" +
                "SELECT ?x ?z WHERE {?x a ub:FullProfessor. ?x ub:zipcode ?z.}\n";

        String query = "prefix ub:    <http://swat.cse.lehigh.edu/onto/univ-bench.owl#>\n" +
                    "INSERT DATA {<http://www.Department12.University0.edu/FullProfessor7> ub:zipcode \"80099\".}";

         */
        FusekiServer server = FusekiServer.create()
                .port(PORT)
                .add(PUBLIC_SERVER, initiatePublicDataset(fileType, nonAnonymizedFile))
                .add(PRIVATE_SERVER, TDBDataset.createTDBDataset(PRIVATE_DATASET, PRIVATE_DIRECTORY))
                .build();


        ArrayList<String> queries = fileReader(args[2]);

        server.start();
        for (var query : queries) {
            System.out.println(query);
            long executeTime = accessServer(server, args[1], PORT, query, checkUserRole(args[0]));
            System.out.println(EvaluationFuseki.showTime(executeTime) + "\n");
        }
        server.stop();
    }

    private static Dataset initiatePublicDataset(String argument, String nonAnonymizedFile) {
        if (argument.equals("-na") || argument.equals("-NA")) {
            return TDBDataset.createTDBDataset(nonAnonymizedFile, PUBLIC_DIRECTORY);
        }
        if (argument.isEmpty() || argument.equals("-a") || argument.equals("-A")) {
            return TDBDataset.createTDBDataset(PUBLIC_DATASET, PUBLIC_DIRECTORY);
        }
        else {
            System.out.println("argument must be -na (non anonymized) or -a (anonymized)");
            return null;
        }
    }

    private static int checkUserRole(String role) {
        if (role.equals("-dev") || role.equals("--developer")) {
            System.out.println("Connect as a developer");
            return 1;
        }
        if (role.equals("-dba")) {
            System.out.println("Connect as a dba");
            return 2;
        }
        System.out.println("argument must be (-dev or --developer or -dba)");
        return 0;
    }

    private static int checkQuery(String query) {
        if (query.contains("SERVICE <http://localhost:3330/public>") && !query.contains("SERVICE <http://localhost:3330/private>")) {
            return 1;
        }
        if (query.contains("SERVICE <http://localhost:3330/private>") && !query.contains("SERVICE <http://localhost:3330/public>")) {
            return 2;
        }
        if (query.contains("SERVICE <http://localhost:3330/private>") && query.contains("SERVICE <http://localhost:3330/public>")) {
            return 0;
        }
        return -1;
    }

    private static long executeQuery(String datasetServer, int port, String query, boolean twoEndpoints) {
        try {
            if (!twoEndpoints) {
                try {
                    String url = "http://localhost:" + port + datasetServer;
                    try (RDFConnection connection = RDFConnectionFactory.connect(url)) {
                        long startTime = System.currentTimeMillis();
                        if (query.contains("SELECT") || query.contains("select")) {
                            connection.queryResultSet(query, ResultSetFormatter::out);
                        }
                        else {
                            connection.update(query);
                        }
                        long stopTime = System.currentTimeMillis();
                        return stopTime - startTime;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    try (RDFConnection connection = RDFConnectionRemote.create()
                            .destination("http://localhost:3330/private")
                            .destination("http://localhost:3330/public")
                            .build()) {
                        long startTime = System.currentTimeMillis();
                        if (query.contains("SELECT") || query.contains("select")) {
                            connection.queryResultSet(query, ResultSetFormatter::out);
                        }
                        else {
                            connection.update(query);
                        }
                        long stopTime = System.currentTimeMillis();
                        return stopTime - startTime;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            return 1;
        }
    }

    private static long accessServer(FusekiServer server, String datasetServer, int port, String query, int role) {
        if (role == 1) {
            if (datasetServer.equals("--public")) {
                if (checkQuery(query) == 2 || checkQuery(query) == 0) {
                    System.out.println("Not allowed access private endpoint. Please check your query");
                    return 0;
                }
                else {
                    return executeQuery(PUBLIC_SERVER, port, query, false);
                }
            }
            if (datasetServer.equals("--private") || datasetServer.equals("--all")) {
                System.out.println("Not allowed to access");
            }
            else {
                System.out.println("datasetSerer must be --public");
            }
        }
        if (role == 2) {
            if (datasetServer.equals("--public")) {
                if (checkQuery(query) == 2 || checkQuery(query) == 0) {
                    System.out.println("Please check your access argument (--private or --all)");
                    return 0;
                }
                else {
                    return executeQuery(PUBLIC_SERVER, port, query, false);
                }
            }
            if (datasetServer.equals("--private")) {
                if (checkQuery(query) == 1 || checkQuery(query) == 0) {
                    System.out.println("Please check your access argument (--public or --all)");
                    return 0;
                }
                else {
                    return executeQuery(PRIVATE_SERVER, port, query, false);
                }
            }
            if (datasetServer.equals("--all")) {
                if (checkQuery(query) == -1) {
                    return executeQuery("", port, query, true);
                }
            }
            else {
                System.out.println("datasetSerer must be --public or --private");
            }
        }
        if (role == 0) {
            System.out.println("Something went wrong, please recheck your arguments");
        }
        return 0;
    }

    private static boolean testQuery(String query) {
        if (query.contains("SELECT") || query.contains("select")) {
            return true;
        }
        if (query.contains("DELETE") || query.contains("delete")) {
            return true;
        }
        if (query.contains("INSERT") || query.contains("insert")) {
            return true;
        }
        return false;
    }

    private static ArrayList<String> fileReader(String path) throws FileNotFoundException {
        try {
            ArrayList<String> queries = new ArrayList<>();
            File file = new File(path);
            String prefix = "";
            String query = "";
            List<String> lines = FileUtils.readLines(file, "UTF-8");
            int i = 0;
            int j = 0;
            while (i < lines.size()) {
                if (lines.get(i).contains("prefix") || lines.get(i).contains("PREFIX")) {
                    prefix = prefix.concat(lines.get(i) + "\n");
                }
                if (testQuery(lines.get(i))) {
                    query = query.concat(lines.get(i) + "\n");
                    j = i + 1;
                    while (!testQuery(lines.get(j))) {
                        query = query.concat(lines.get(j) + "\n");
                        j++;
                        if (j >= lines.size()) {
                            break;
                        }
                    }
                    if (!query.isEmpty()) {
                        queries.add(prefix + query);
                        query = "";
                    }
                    i = j - 1;
                }
                i++;
            }
            return queries;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
