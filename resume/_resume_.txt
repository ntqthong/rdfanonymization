### Sypse: Privacy-first Data Management through Pseudonymization and Partitioning ###

Les responsabilités dues a l'utilisation de nombreuses données personnelles privées ne cesse de grandir, et le public a de moins en moins 
confiance aux entreprises sur leurs manières de gérer ces données. Les systèmes de données actuels ont été crées de sorte à simplifier le 
stockage, l'accès, le traitement et le partage. Une vision transparente est donc proposée afin de réarchitecturer les bases de données actuelles
en combinant la pseudonymisation, les données synthétiques et le partitionnement de données afin d'atteindre 3 buts:
 - séparer les informations détaillés personnelles (date de naissance, code postal, ville...) des informations d'identification personelles nommées
 PII (nom, prénom) et les mélanger afin de réduire l'impact des brèches
 - simplifier les demandes de suppression en écrivant par dessus des portions de données
 - limiter l'accès des PII aux développeurs et ingénieurs

 Cette vision transparente se nomme "Sypse" et se base donc sur les 3 points cités ci-dessus:
  - la pseudonymisation qui consiste à à séparer d'une donnée ses champs d'identifications personnelles
  - les données synthétiques, qui seront des données générés servant à du débeuguage ou de l'analyse de performance,remplacant ainsi les données
pseudonymisés durant ces tâches
  - le partitionnement de données afin de limiter les fuites de donnés et de simplifier les requêtes de suppression de données

Concernant l'architecture de Sypse, l'idée principale est de partitionner les données en deux ou plusieurs partitions, en introduisant des données
synthétiques de des identifiants pseudonymisés afin de permettre un accès différent à la donnée. En partant sur deux partitions, nous avons
la base de données détaillés (Detail database) et la base de données d'informations d'identification personnelles (PII Database).
Detail database contiendra donc les données pseudonymisés. Une exigence clé de Sypse est que les bases de données sous-jacentes soient 
administrativement séparées, ont des contrôles d'accès différents et soient sauvegardées indépendamment.


### Controlling Access to RDF Graphs ###

RDF étant devenu une représentation standard des données du web, malgré son développement, il manque un système permettant le contrôle d'accès à
ces données. Ce qui nous est proposé ici, c'est que sur des requêtes d'opérations de lecture seule, les triplets présents dans la base se voient
attribués une annotation indiquant si ils sont accessibles où non au moment de la lecture. Cela requiert une permission de contrôle d'accès de haut
niveau. Et cette mise en place se fait via la notion de triples patterns de SPARQL ((U) x (U) x (U u L)) en ajoutant un ensembe de variables V
disjoints des variables U et L ce qui donne ((V u U) x (V u U) x (V u U u L)).
